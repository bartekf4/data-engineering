# I10. Trees in San Francisco

## Logs

Who did what :)

- Bartłomiej Wajdzik
  - Pulling data from OSM, Data Cleaning (OSM data) and merging with trees Dataset, writing little adjustments to documentation and writing description in first notebook
- Bartosz Fudali
  - Tree cleaning, ML scripts, final touches, markdown text correction
- Filip Kubiś
  - Documentation and friendly help with scripts above

## Key points

-->[Link](https://gitlab.com/bartekf4/data-engineering/-/blob/main/%5B01%5D-Data_cleaning_and_production.ipynb)
to our jupyter notebook


## Repo files justification

We decided to leave all notebooks from script, since they keep outputs so that every little step can be carefully examined with proper result.
We also left all the necessary csv files to give data for the algorithms to work with. Especially that they contain everything what is needed to work.
Lastly we delivered scripts with markdown files so you can choose whether to run it, or read code with markdown without any output.
