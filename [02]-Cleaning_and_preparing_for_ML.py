# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + pycharm={"name": "#%%\n"}
import pandas as pd

df = pd.read_pickle("Tree_shadows_on_building_San_Francisco_RAW.pkl")

# + [markdown] pycharm={"name": "#%% md\n"}
# We create a column with the percentage of shaded area of the building.

# + pycharm={"name": "#%%\n"}
df["building_surface_area"] = df["building_surface"].area
df["shadow_area"] = df['shadow'].area
df["percents_of_shaded_area"] = (df["shadow_area"] / df["building_surface_area"])
df

# + pycharm={"name": "#%%\n"}
df.drop(columns=["shadow", "shadow_area", "building_surface_area"], inplace=True)
# -

# Bearing in mind the fact, that some of the buildings are not shaded at all we change the Nan value,
# which represents in our DataFrame no shade, to 0.

# + pycharm={"name": "#%%\n"}
df['percents_of_shaded_area'] = df['percents_of_shaded_area'].fillna(0)
df
# -

# It’s high time to change the representation of the building in the DataFrame. Instead of
# shapely.geometry.polygon.Polygon we are adding two auxiliary columns, which will contain the longitude and latitude
# of the center of the building. In order to manage it, first we calculate the center of the polygon. Later on we
# process the data and the necessary information is added to DataFrame.v

# + pycharm={"name": "#%%\n"}
df["building_surface_center_lon"] = df['building_surface'].centroid.x
df["building_surface_center_lat"] = df['building_surface'].centroid.y
df
# -

# As the column <code> "building_surface" </code> is no longer needed, it is dropped.

# + pycharm={"name": "#%%\n"}
df.drop(columns=["building_surface"], inplace=True)

# + pycharm={"name": "#%%\n"}
df.rename(columns={"building_surface_center_lon": "building_lon", "building_surface_center_lat": "building_lat"},
          inplace=True)

# + [markdown] pycharm={"name": "#%% md\n"} We change the value of column *green_area_near* from <code> bool </code>
# to <code> int </code> (*False* = 0, *True* = 1)

# + pycharm={"name": "#%%\n"}
df['green_area_near'] = df['green_area_near'].astype(int)

# + [markdown] pycharm={"name": "#%% md\n"}
# We've manage to prepare the data for further processing, for instance machine learning.

# + pycharm={"name": "#%%\n"}
df.to_csv('Tree_shadows_on_building_San_Francisco.csv', index=False)

# + pycharm={"name": "#%%\n"}
df
