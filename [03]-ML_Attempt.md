---
jupyter:
jupytext:
text_representation:
extension: .md format_name: markdown format_version: '1.3' jupytext_version: 1.13.8 kernelspec:
display_name: Python 3 language: python name: python3
---

We read the DataFrame from pickle.

```python
import pandas as pd

df = pd.read_csv("Tree_shadows_on_building_San_Francisco.csv")
df
```

<!-- #region pycharm={"name": "#%% md\n"} -->
We'll try a few different models to predict the data and mainly to check the data as far as it's usability is concerned.
<!-- #endregion -->

```python pycharm={"name": "#%%\n"}
from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()

df[["building_lat", 'building_lon']] = scaler.fit_transform(df[["building_lat", 'building_lon']])
df
```

```python pycharm={"name": "#%%\n"}
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(df[['building_lon', 'building_lat', 'green_area_near']],
                                                    df[['percents_of_shaded_area']],
                                                    test_size=.2, random_state=420, shuffle=True)
```

```python pycharm={"name": "#%%\n"}
from sklearn.linear_model import SGDRegressor

model = SGDRegressor(random_state=420)

model.fit(X_train, y_train.values.ravel())

print(f'SGD score: {model.score(X_test, y_test)}')
```

```python pycharm={"name": "#%%\n"}
from sklearn.linear_model import LassoCV

lasso = LassoCV()

lasso.fit(X_train, y_train.values.ravel())

print(f'lasso score: {lasso.score(X_test, y_test)}')
```

```python pycharm={"name": "#%%\n"}
y_test
```

```python pycharm={"name": "#%%\n"}
lasso.predict(X_test)
```

```python pycharm={"name": "#%%\n"}
from sklearn.linear_model import ElasticNetCV

en = ElasticNetCV()

en.fit(X_train, y_train.values.ravel())

en.score(X_test, y_test)
print(f'ElasticNetCV score: {en.score(X_test, y_test)}')
```

```python pycharm={"name": "#%%\n"}
from sklearn.linear_model import RidgeCV

ridge = RidgeCV()

ridge.fit(X_train, y_train.values.ravel())
ridge.score(X_test, y_test)
print(f'RidgeCV: {ridge.score(X_test, y_test)}')
```
