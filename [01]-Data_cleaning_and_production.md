---
jupyter:
jupytext:
cell_metadata_filter: -all formats: ipynb,py:light,md text_representation:
extension: .md format_name: markdown format_version: '1.3' jupytext_version: 1.13.8 kernelspec:
display_name: Python 3 (ipykernel)
language: python name: python3
---

### Cleaning and obtaining building data

First, we are loading data from 'Building_SF.csv' to the DataFrame. The file contains data about buildings in San
Francisco and additional information about green areas, such as parks and gardens. We have choosen for this task pyarrow
engine since it is blazingly fast

```python
import geopandas as gpd
import pandas as pd

final = pd.read_csv('Buildings_SF.csv', delimiter=',', header=0, index_col=['element_type', 'osmid'], engine='pyarrow')
```

```python
final
```

We drop the data which we reckon as unnecessary, we keep columns such as *building polygons*, *building* and *location*.
They'll be in handy to get rid of the unwanted structures as well as distinguish green areas.

```python
final = final.query("element_type in ['way','relation']")
final.reset_index(drop=True, inplace=True)
final = final[['building_shape', 'building', 'location', 'leisure', 'natural', 'landuse']]
```

```python
final
```

We need to extract the parks and gardens from our database into a new DataFrame.

```python
forest = final.query("natural == 'wood' or leisure in ['garden','park'] or landuse == 'forest'")
final.drop(forest.index, inplace=True)
```

```python
final
```

```python
forest
```

As mentioned before, we are filtering buildings to remove objects marked as buildings, but are not ones. All floating
objects are removed, as well as tents and construction sites.

```python
final.drop(
  final[final.building.map(lambda x: str(x) in ['houseboat', 'construction', 'tent', 'mobile_home', 'ship'])].index
  , inplace=True)
final.drop(final[final.landuse == 'construction'].index, inplace=True)
```

```python
final
```

Underground structures should also be dropped since trees can't cast a shadow on them.

```python
final.drop(final[final.location.map(lambda x: str(x) == 'underground')].index, inplace=True)
```

```python
final
```

We drop additional labels so that the only column left is *polygon*.

```python
final.drop(columns=['building', 'location', 'natural', 'landuse', 'leisure'], inplace=True)
forest.drop(columns=['building', 'location', 'natural', 'landuse', 'leisure'], inplace=True)
```

```python
final
```

```python
forest
```

The time has come to use geometrical data. The first step is to translate objects loaded from csv to polygons.

```python
geom = gpd.GeoSeries().from_wkt(final.building_shape)
forest_geom = gpd.GeoSeries().from_wkt(forest.building_shape)
final = gpd.GeoDataFrame(final, geometry=geom, crs='epsg:4326')
forest = gpd.GeoDataFrame(geometry=forest_geom, crs='epsg:4326')
final.drop(columns=['building_shape'], inplace=True)
forest.rename(columns={'geometry': 'forest_shape'}, inplace=True)
final.rename(columns={'geometry': 'building_shape'}, inplace=True)
```

```python
forest
```

```python
final.set_geometry('building_shape', inplace=True)
forest.set_geometry('forest_shape', inplace=True)
```

Later on we create a square polygon to use later to dispose of data from distant islands.

```python
from shapely.geometry import Polygon
from shapely import speedups

speedups.enable()
border = Polygon([[-122.55, 37.7], [-122.55, 37.83], [-122.3, 37.83], [-122.3, 37.7]])
```

```python
mask = final.within(border)
final = final.loc[mask]
mask = forest.within(border)
forest = forest.loc[mask]
```

Before matching a building with a nearby park, we have to change the epsg to a local one.

```python
final.to_crs(epsg=7131, inplace=True)
forest.to_crs(epsg=7131, inplace=True)
```

As our forest data consists of various types of garden, parks, woods which tends to interept one another, it's
recommended to join intercepting poligons into one. In order to do so, we firstly make one big multipoligon with **
unary_union**, followed by **explode** dividing it once again but into separated ones.

```python
forest = gpd.GeoDataFrame(geometry=[forest.unary_union]).explode(index_parts=False).reset_index(drop=True).set_crs(
  epsg=7131)
```

Now our main goal, is to find appropriate distance so we can decide whether building is close to green area or not.

First step is to find nearest building for each green area. Secondly we should extract distance, but as we can see
attribute **distance_col** does it for us.

```python
forest = forest.sjoin_nearest(final, how='left', distance_col='distance')
```

Our distance will be the mean of calculated distances. Since spatial joining created some duplicates we have to remove
them and drop columns created in that process (the are not needed now)

```python
mean_distance_from_green_area = forest['distance'].mean()
forest.drop(columns=['distance', 'index_right'], inplace=True)
forest.drop_duplicates(inplace=True)
```

```python
forest
```

We are now matching buildings with parks that are less than calculated distance away from them.

```python
final = final.sjoin_nearest(forest, how='left', max_distance=mean_distance_from_green_area)
green_area_near = final.index_right.groupby(level=0).count() > 0
final = final[~final.index.duplicated()]
final = final.merge(green_area_near, left_index=True, right_index=True)
green_area_near = None
```

Final set is done, and forest is removed (to free up memory)

```python
final.rename(columns={'index_right_y': 'green_area_near'}, inplace=True)
final.drop(columns=['index_right_x'], inplace=True)
forest = None
```

```python
final
```

### Cleaning Tree Data

First, we'll be loading the data from the csv file.

```python
trees = gpd.GeoDataFrame().from_file("Urban_Tree_Canopy.csv")
```

Since the newly created geoDataFrame in the 'the_geom' column contains only strings, we are converting those strings
into proper polygons.

```python
trees["the_geom"] = gpd.GeoSeries.from_wkt(trees["the_geom"])
```

```python
trees.set_geometry("the_geom", inplace=True)
```

Now we need to drop all of the unnecessary columns.

```python
trees.drop('NAME', inplace=True, axis=1)
trees.drop('T_SQFT', inplace=True, axis=1)
trees.drop('ACRES', inplace=True, axis=1)
trees.drop('geometry', inplace=True, axis=1)
```

Renaming columns so that they indicate content.

```python
trees.rename(columns={"the_geom": "tree polygons"}, inplace=True)
```

First we need to set epsg to global, since data collected in the csv file operates on it. Our next step is to change the
espg to a proper one.

```python
trees.set_geometry("tree polygons", inplace=True)
trees.set_crs(epsg=4326, inplace=True, allow_override=True)
trees.to_crs(epsg=7131, inplace=True)
```

```python
trees
```

```python
final
```

### Merging data together

Our plan is to calculate intersection polygons of shadows on the building surface, and append that result to the
building DataFrame.

We need to make geospatial joins to match buildings with their intersecting trees.

```python
final_shadows = final.sjoin(trees, how='inner', predicate='intersects')
```

```python
final_shadows
```

Since null values throw errors and are irrelevant, we drop them.

```python
final_shadows.index.names = ['ID']
```

```python
final_shadows
```

We're now joining our table with a tree table in order to obtain polygons. Also we are dropping **index_right** column
since it's useless later

```python
final_shadows = final_shadows.join(trees, on='index_right').drop('index_right', axis=1).set_geometry('tree polygons')
```

```python
final_shadows
```

Dropping geometry is also useless.

```python
final_shadows.drop('building_shape', inplace=True, axis=1)
```

```python
final_shadows
```

Since previous attempts to marge trees' polygons failed, we need to use buffer(0) whic fixes poligons

```python
final_shadows['tree polygons'] = final_shadows['tree polygons'].buffer(0)
```

Now we are making union of trees that overlaps the same building

```python
final_shadows = final_shadows.dissolve('ID')
```

```python
final_shadows
```

At the end we are getting intersection area of building with trees which produces ours desired shadow. The result is
appended to building table which now contains all required data

```python
final['shadow'] = final['building_shape'].intersection(final_shadows['tree polygons'], align=True)
```

```python
final.rename(columns={"building_shape": 'building_surface'}, inplace=True)
final
```

Little presentation of how our data looks like. As a sample we will choose buildings within small random border.

Green shapes are buildings and there black taints on them are actual shadow polygons on the building

```python
final.set_geometry('building_surface', inplace=True)
border = Polygon([[49800, 27500], [50400, 27500], [50400, 27800], [49800, 27800]])
show = final.loc[final.within(border)]
```

```python
from matplotlib import pyplot as plt

fig, ax = plt.subplots(1, 1, figsize=(20, 20))
show.building_surface.plot(ax=ax, color='g')
show.shadow.plot(ax=ax, color='black')
```

```python
final.to_pickle('Tree_shadows_on_building_San_Francisco_RAW.pkl')
```
