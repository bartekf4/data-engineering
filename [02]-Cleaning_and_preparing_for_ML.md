---
jupyter:
jupytext:
text_representation:
extension: .md format_name: markdown format_version: '1.3' jupytext_version: 1.13.8 kernelspec:
display_name: Python 3 language: python name: python3
---

```python pycharm={"name": "#%%\n"}
import pandas as pd

import geopandas as gpd
from shapely import wkt

df = pd.read_pickle("Tree_shadows_on_building_San_Francisco_RAW.pkl")

```

<!-- #region pycharm={"name": "#%% md\n"} -->
We create a column with the percentage of shaded area of the building.
<!-- #endregion -->

```python pycharm={"name": "#%%\n"}
df["building_surface_area"] = df["building_surface"].area
df["shadow_area"] = df['shadow'].area
df["percents_of_shaded_area"] = (df["shadow_area"] / df["building_surface_area"])
df
```

```python pycharm={"name": "#%%\n"}
df.drop(columns=["shadow", "shadow_area", "building_surface_area"], inplace=True)
```

Bearing in mind the fact, that some of the buildings are not shaded at all we change the Nan value, which represents in
our DataFrame no shade, to 0.

```python pycharm={"name": "#%%\n"}
df['percents_of_shaded_area'] = df['percents_of_shaded_area'].fillna(0)
df
```

It’s high time to change the representation of the building in the DataFrame. Instead of
shapely.geometry.polygon.Polygon we are adding two auxiliary columns, which will contain the longitude and latitude of
the center of the building. In order to manage it, first we calculate the center of the polygon. Later on we process the
data and the necessary information is added to DataFrame.v

```python pycharm={"name": "#%%\n"}
df["building_surface_center_lon"] = df['building_surface'].centroid.x
df["building_surface_center_lat"] = df['building_surface'].centroid.y
df
```

As the column <code> "building_surface" </code> is no longer needed, it is dropped.

```python pycharm={"name": "#%%\n"}
df.drop(columns=["building_surface"], inplace=True)
```

```python pycharm={"name": "#%%\n"}
df.rename(columns={"building_surface_center_lon": "building_lon", "building_surface_center_lat": "building_lat"},
             inplace=True)
```

<!-- #region pycharm={"name": "#%% md\n"} -->
We change the value of column *green_area_near* from <code> bool </code> to <code> int </code> (*False* = 0, *True* = 1)
<!-- #endregion -->

```python pycharm={"name": "#%%\n"}
df['green_area_near'] = df['green_area_near'].astype(int)
```

<!-- #region pycharm={"name": "#%% md\n"} -->
We've manage to prepare the data for further processing, for instance machine learning.
<!-- #endregion -->

```python pycharm={"name": "#%%\n"}
df.to_csv('Tree_shadows_on_building_San_Francisco.csv', index=False)
```

```python pycharm={"name": "#%%\n"}
df
```
